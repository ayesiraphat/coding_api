module.exports = function(app){
    app.post("/Plus",function(req,res){
        //INPUT
        console.log('Plus');
        var a = parseInt(req.body.A)
        var b = parseInt(req.body.B)
        console.log('A :'+a);
        console.log('B :'+b);

        if(isNaN(a)||isNaN(b)){ //ถ้าไม่ใช่ตัวเลขให้ทำ
            console.log('A or B is not number. Please try again')
            var messageErr = {
                A : a,
                B : b,
                Result : "A or B is not number. Please try again"
            }
            res.json(messageErr)
        }else{
            console.log('A :'+a);
            console.log('B :'+b);
            //LOGIC
            var result = a+b;
            console.log("Result "+a+"+"+b+" : "+result);
            //OUTPUT
            var resAsjson ={
               A : a,
               B : b,
               Result : result
            }
            res.json(resAsjson) //ส่งข้อมูล
        }
    })

    app.post("/multiplied_by",function(req,res){
        //INPUT
        console.log('Multiplied by');
        var a = req.body.A
        var b = req.body.B
        if(isNaN(a)||isNaN(b)){ //ถ้าไม่ใช่ตัวเลขให้ทำ
            console.log('A or B is not number. Please try again')
            var messageErr = {
                A : a,
                B : b,
                Result : "A or B is not number. Please try again"
            }
            res.json(messageErr)
        }else{
        console.log('A :'+a);
        console.log('B :'+b);
        //LOGIC
        var result = a*b;
        console.log("Result "+a+"*"+b+" : "+result);
        //OUTPUT
        var resAsjson ={
           A : a,
           B : b,
           Result : result
        }
        res.json(resAsjson)
        }
    })
    app.get("/minus/:A/:B",function(req,res){
        console.log('Minus');
        //INPUT
        var a =req.params.A
        var b =req.params.B
        if(isNaN(a)||isNaN(b)){ //ถ้าไม่ใช่ตัวเลขให้ทำ
            console.log('A or B is not number. Please try again')
            var messageErr = {
                A : a,
                B : b,
                Result : "A or B is not number. Please try again"
            }
            res.json(messageErr)
        }else{
        console.log('A :'+a);
        console.log('B :'+b);
        //LOGIC
        var result = a-b;
        console.log("Result : "+a+"-"+b+" : "+result);
        //OUTPUT
        var resAsjson ={
            A : a,
            B : b,
            Result : result
        }
        res.json(resAsjson)
        }
    })
    app.get("/devided_by/:A/:B",function(req,res){
        console.log('Devided By')
        //INPUT
        var a =req.params.A
        var b =req.params.B
        if(isNaN(a)||isNaN(b)){ //ถ้าไม่ใช่ตัวเลขให้ทำ
            console.log('A or B is not number. Please try again')
            var messageErr = {
                A : a,
                B : b,
                Result : "A or B is not number. Please try again"
            }
            res.json(messageErr)
        }else{
        console.log('A :'+a);
        console.log('B :'+b);
        //LOGIC
        var result = a/b;
        console.log("Result :"+a+"/"+b+" : "+result);
        //OUTPUT
        var resAsjson ={
            A : a,
            B : b,
            Result : result
        }
        res.json(resAsjson)
        }
    })
}